var connect = require('connect'),
    http = require('http'),
    markdown = require('marked'),
    fs = require('fs'),
    path = require('path');

var app = connect()
  .use(connect.logger('dev'))
  .use(connect.static('web'))
  .use(function(req, res){
    fs.readFile(path.join(__dirname, 'README.markdown'), function (err, md) {
        if (err) return res.end(err);
        var html = [
            '<!DOCTYPE HTML>',
            '<html><head><link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"/></head><body>',
            '<div style="width: 960px; margin: auto">',
            markdown('' + md),
            '</div>',
            '</body></html>'
        ].join('');

        res.end(html);
    });
  });

http.createServer(app).listen(process.env.PORT || 3000);
