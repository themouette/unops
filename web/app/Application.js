Ext.define('Life.Application', {
    name: 'Life',

    extend: 'Ext.app.Application',

    requires: [
        'Life.lib.Mediator',
        'Life.lib.Logger',
        'Life.lib.Simulation'
    ],

    views: [
        // TODO: add views here
    ],

    controllers: [
        'Simulation',
        'Log',
        'Chart'
    ],

    stores: [
        'Logs',
        'Organisms'
    ],

    launch: function () {

        // Create a new logger for this sequence
        var logger = Ext.create('Life.lib.Logger', {
            store: Ext.data.StoreManager.lookup('logs'),
            namespace: 'application'
        });

        console.log('lauched');
    }
});

// This is the configuration loading.
// It should not be defined here, I am sure I would find the right way when time comes.

console.log([
    '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n',
    '\n',
    'DISCLAIMER\n',
    '==========\n',
    '\n',
    'Configuration is loaded the hard way.\n',
    'If I have enough time, I will look further on the best way ',
    'to do it.\n',
    '>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n'
].join(''));

// Create a logs store.
//
// Simply access to it using
//
// ``` javascript
// Ext.data.StoreManager.lookup('logs');
// ```
var logs = Ext.create('Life.store.Logs', {
    storeId: 'logs'
});
// Create the simulation main.
//
// As there is no DI container (or I did not find it), it is exposed
// globally under Life.simulation.
//
// Simply access to it using
//
// ``` javascript
// Life.simulation
// ```
var simulation = Life.simulation = Ext.create('Life.lib.Simulation', {
    logStore: Ext.data.StoreManager.lookup('logs'),
    populations: [{
        name: 'grass',
        birthRate: 50,
        lifeExpectancy: 2000,
        eat: ['bacteria']
    }, {
        name: 'zebra',
        birthRate: 10,
        lifeExpectancy: 2000,
        eat: ['grass']
    }, {
        name: 'lion',
        birthRate: 300,
        lifeExpectancy: 2000,
        eat: ['zebra']
    }, {
        name: 'bacteria',
        birthRate: 10,
        lifeExpectancy: 2000,
        eat: ['lion']
    }]
});
