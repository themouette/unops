// `Life.lib.Population`
//
// Communicate with simulation through mediator.
//
// Each population use it's name to communicate on the mediator.
//
// ``` javascript
// Ext.create('Life.lib.Population', {
//     name: 'zebra',
//     birthDelay: 50,
//     lifeExpectancy: 2000,
//     eat: ['grass'],
//     // Mediator to use for communication
//     mediator: Ext.create('Life.lib.Mediator'),
//     // Store to put logs in
//     logStore: Ext.data.StoreManager.lookup('logs')
// });
// ```
//
// Following events are pushed to mediator to handle organisms lifecycles:
//
// * birth: a new organism of this type is born
// * eatable: an organism of this population became eatable
// * die: an organism of this population died.
//
// All callbacks have the following signature:
//
// `function (organism, population) {}`
//
// `countchange` is fired on any population count change.
// It expects callback with this signature: `function (newCount, population)`
Ext.define('Life.lib.Population', {
    requires: [
        'Life.model.Organism',
        'Life.store.Organisms'
    ],

    constructor: function (config) {
        // copy properties
        this.name = config.name;
        this.birthDelay = config.birthRate;
        this.lifeExpectancy = (config.lifeExpectancy || 2000);
        this.eat = config.eat || [];

        var mediator = this.mediator = config.mediator;

        // Create a new logger for this sequence
        var logger = this.logger = Ext.create('Life.lib.Logger', {
            store: config.logStore,
            namespace: this.name
        });
        mediator
            .registerEvent(this.name, 'birth')
            .registerEvent(this.name, 'eatable')
            .registerEvent(this.name, 'die')
            .registerEvent(this.name, 'countchanged')

            .addListener('simulation', 'start', this.simulationStartListener.bind(this))
            .addListener('simulation', 'stop', this.simulationStopListener.bind(this))
            .addListener('simulation', 'pause', this.simulationPauseListener.bind(this))
            .addListener('simulation', 'unpause', this.simulationUnpauseListener.bind(this))
            .addListener('simulation', 'unpause', this.simulationUnpauseListener.bind(this))

        ;

        this.store = Ext.create('Life.store.Organisms');

        // start if simulation is already started
        if (simulation.isStarted()) {
            this.simulationStartListener();
        }
    },

    count: function () {
        return this.store.count();
    },

    isExtinct: function () {
        return this.count() <= 0;
    },

    // ## A bunch of listeners for application flow.
    simulationStartListener: function () {
        this.giveBirth();
        this.setupBirthDelegation();
        this.setupEatDelegation();
    },
    simulationStopListener: function () {
        this.stopBirthDelegation();
    },
    simulationPauseListener: function () {
        this.setupBirthDelegation();
    },
    simulationUnpauseListener: function () {
        this.stopBirthDelegation();
        this.stopEatDelegation();
    },

    // ## Helper

    publishCountChange: function () {
        this.mediator.fireEvent(this.name, 'countchanged', [this.count(), this]);
    },

    // ## Birth management

    giveBirth: function () {

        var organism = Ext.create('Life.model.Organism', {
            lifeExpectancy: this.lifeExpectancy,
            name: this.name
        });
        organism.listenToSimulationChange(this.mediator);
        var name = this.name;

        // Listen to organism eatable event.
        // Event is forwarded to mediator, then logged.
        organism.addManagedListener(organism, 'becomeEatable', function (organism) {
                // log the event.
                this.logger.log('eatable');
                // publish the 'eatable event'
                this.mediator.trigger(name, 'eatable', organism, this);
            }.bind(this));

        // Listen to organism die event.
        // Event is forwarded to mediator, then logged.
        organism.addManagedListener(organism, 'die', function (organism) {
                this.store.remove(organism);
                this.mediator.trigger(name, 'die', organism, this);
                this.logger.log('die');
                this.publishCountChange();
            }.bind(this));

        // Add element to store
        this.store.add(organism);

        // Publish birth.
        this.mediator.trigger(name, 'birth', organism, this);
        this.logger.log('birth');
        this.publishCountChange();

        return this;
    },

    setupBirthDelegation: function() {
        this.birthTimer = setTimeout(function () {
                if (this.isExtinct()) {
                    this.logger.log('Population extinct');
                    return this;
                }

                this.giveBirth();

                // program next round
                this.setupBirthDelegation();
            }.bind(this), this.birthDelay);
    },
    stopBirthDelegation: function () {
        clearTimeout(this.birthTimer);
    },

    // ## Feeding management

    isEatable: function (organism) {
        return true;
    },

    isCaught: function (organism) {
        return Math.random() < 0.5;
    },

    // callback bound to every eatable population `eatable`.
    listenToBecomeEatable: function (organism) {
        if (this.isExtinct()            ||
            organism.isDead()           ||
            !this.isEatable(organism)   ) {

            return this;
        }

        if (!this.isCaught(organism)) {
            this.logger.log('Did not catch organism ' + organism.get('name'));
            return this;
        }

        this.logger.log('Eat organism ' + organism.get('name'));
        this.eatOrganism(organism);
    },

    eatOrganism: function (organism) {
        organism.die();
    },

    setupEatDelegation: function () {
        this.eat.forEach(function (name, index) {
            this.mediator.addListener(name, 'eatable', this.listenToBecomeEatable.bind(this));
        }.bind(this));
    },

    // TODO requires a removeListener on mediator.
    stopEatDelegation: function () {
        console.log('Eat delegation stop is not implemented yet.');
    }
});
