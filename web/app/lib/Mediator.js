// The Mediator object is an event broker for in app communication.
//
// It defines custom observables organised into namespaces.
//
// ``` js
// var mediator = Ext.create('Life.lib.Mediator');
// mediator
//     .registerEvent('plane', 'takeOff')
//     .registerEvent('truck', 'start')
//     .registerEvent('truck', 'thunder');
// ```
(function (Ext) {
    var arrayProto = Array.prototype,
        arraySlice = arrayProto.slice;

    Ext.define('Life.lib.Mediator', {
        // Relies on Observables
        requires: [
            'Ext.util.Observable'
        ],

        // Initialize namespaces collection
        constructor: function () {
            // create event list.
            this.namespaces = {};
        },

        // Create a new namespaced event.
        //
        // It is required to declare an event before using it or defining
        // listeners.
        //
        // @param String    namespace   the event name space
        // @param String    eventname   the event name. Optional.
        //
        // @return Life.lib.Mediator
        registerEvent: function(namespace, eventname) {
            var observable = observableFromNs(this, namespace);
            // ensure namespace is created
            if (!observable) {
                observable = createNamespaceObservable(this, namespace);
            }

            // Register event into dedicated NS.
            if (eventname) {
                observable.addEvents(eventname);
            }

            return this;
        },
        // Add a new listener for namespace:event.
        addListener: function(namespace, eventname, callback) {
            var observable = observableFromEventOrError(this, namespace, eventname);

            observable.on.apply(observable, arraySlice.call(arguments, 1));

            return this;
        },
        // trigger an event.
        // extra arguments are forwarded.
        fireEvent: function(namespace, eventname) {
            var observable = observableFromEventOrError(this, namespace, eventname);

            observable.fireEventArgs.apply(observable, arraySlice.call(arguments, 1));

            return this;
        }
    });

    Life.lib.Mediator.createAlias({
        trigger: 'fireEvent',
        on: 'addListener'
    });

    // Leverage Javascript Hoisting to define private methods **after**
    // public interface is declared.
    function createNamespaceObservable(mediator, namespace) {
        var observable = mediator.namespaces[namespace] = Ext.create('Ext.util.Observable');

        return observable;
    }

    function observableFromNs(mediator, namespace) {
        return mediator.namespaces[namespace];
    }

    // Retrieve observable for given namespace.
    // If no observable is found,
    function observableFromEventOrError(mediator, namespace, eventname) {
        var observable = observableFromNs(mediator, namespace);

        // ensure namespace is created
        if (!observable) {
            throw new Error('['+ Ext.getDisplayName(arguments.callee) +'] Namespace '+namespace+' is not defined.');
        }

        // check event is defined in related observable.
        // Check [source code
        // ](http://docs.sencha.com/extjs/4.2.1/source/Observable.html#Ext-util-Observable-method-addEvents)
        if (!(eventname in observable.events)) {
            throw new Error('['+ Ext.getDisplayName(arguments.callee) +'] Event '+namespace+':'+eventname+' is not defined.');
        }

        return observable;
    }

    function ensureEventExists(argument) {

    }

})(Ext);
