// Life.lib.Logger is the logging service.
//
// Use it to log events.
//
// ``` javascript
// var logger = Ext.create('Life.lib.Logger', {
//     store: Ext.data.StoreManager.lookup('logs')
// });
//
// logger.log('tardis', 'varnish');
// ```
//
// Or, define default namespace
//
// ``` javascript
// var logger = Ext.create('Life.lib.Logger', {
//     namespace: 'tardis',
//     store: Ext.data.StoreManager.lookup('logs')
// });
//
// logger.log('varnish');
// ```
Ext.define('Life.lib.Logger', {

    require: [
        'Life.store.Logs'
    ],

    constructor: function (config) {
        if (!config || !config.store) {
            // If no store is provided, throw an exception.
            throw new Error('['+ Ext.getDisplayName(arguments.callee) +'] You must provide a store to logger.');
        } else {
            this.store = config.store;
        }

        if (config && config.namespace) {
            this.namespace = config.namespace;
        }
    },

    // Log a new event in the log store.
    log: function (namespace, eventname) {
        if (this.namespace && arguments.length < 2) {
            eventname = namespace;
            namespace = this.namespace;
        }
        this.store.add({
            namespace: namespace,
            eventname: eventname,
            timestamp: new Date()
        });
    }
});
