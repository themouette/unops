// `Life.lib.Simulation` is the service that handle life simulation.
//
// ``` javascript
// var simulation = Ext.create('Life.lib.Simulation', {
//     logStore: Ext.data.StoreManager.lookup('logs')
// })
//
// simulation.start();
// ```
(function (Ext) {

    var arrayProto = Array.prototype,
        arraySlice = arrayProto.slice;

    Ext.define('Life.lib.Simulation', {

        requires: [
            'Life.lib.Logger',
            'Life.lib.Mediator',
            'Life.lib.Population',
            'Life.model.Statistics'
        ],

        constructor: function (config) {
            var logStore;
            if (!config || !config.logStore) {
                // If no store is provided, throw an exception.
                throw new Error('['+ Ext.getDisplayName(arguments.callee) +'] You must provide the `logStore` to Simulation.');
            } else {
                logStore = this.logStore = config.logStore;
            }
            // create a logger
            this.logger = Ext.create('Life.lib.Logger', {
                store: logStore,
                namespace: 'simulation'
            });

            this.populationConfig = config.populations || [];

            // and a mediator
            this.mediator = Ext.create('Life.lib.Mediator');
            this.mediator
                .registerEvent('simulation', 'start')
                .registerEvent('simulation', 'stop')
                .registerEvent('simulation', 'pause')
                .registerEvent('simulation', 'unpause')
                ;

            this.initStateMachine();

            this.populations = [];
        },

        // ## Population management

        addPopulation: function (config) {
            config.logStore = this.logStore;
            config.mediator = this.mediator;

            var population = Ext.create('Life.lib.Population', config);
            this.populations.push(population);

            this.log('Added population ' + config.name);
            return this;
        },

        getStatistics: function () {
            var stat = {
                timestamp: new Date()
            };
            this.populations.forEach(function(pop) {
                stat[pop.name] = pop.count();
            });
            return Ext.create('Life.model.Statistics', stat);
        },

        // ## Mediator interface

        trigger: function () {
            var args = ['simulation'].concat(arraySlice.call(arguments));
            this.mediator.fireEvent.apply(this.mediator, args);
            return this;
        },

        on: function () {
            var args = ['simulation'].concat(arraySlice.call(arguments));
            this.mediator.addListener.apply(this.mediator, args);
            return this;
        },

        // ## Logger interface

        log: function () {
            this.logger.log.apply(this.logger, arguments);
            return this;
        },

        // ## State Machine

        initStateMachine: function () {
            this.state = {
                started: false,
                pause: false
            };
        },

        isStarted: function () {
            return this.state.started;
        },

        isPaused: function () {
            return this.state.started && this.state.pause;
        },

        start: function () {
            this.populationConfig.forEach(this.addPopulation.bind(this));
            this.state.started = true;
            this.state.pause = false;
            this.log('start');
            this.trigger('start');
            return this;
        },

        pause: function () {
            this.state.pause = true;
            this.trigger('pause');
            return this;
        },

        unpause: function () {
            this.state.pause = false;
            this.trigger('unpause');
            return this;
        },

        togglePause: function () {
            if (this.state.pause) {
                return this.unpause();
            }

            return this.pause();
        },

        stop: function () {
            this.state.started = false;
            this.state.pause = false;
            this.trigger('stop');
            this.log('stop');
            return this;
        }
    });
})(Ext);
