Ext.define('Life.controller.Simulation', {
    extend: 'Ext.app.Controller',

    requires: [
        'Life.view.simulation.Panel'
    ],

    models: [
        'Organism'
    ],

    views: [
        'simulation.Panel'
    ]

});
