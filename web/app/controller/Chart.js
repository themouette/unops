Ext.define('Life.controller.Chart', {
    extend: 'Ext.app.Controller',

    views: [
        'chart.Panel'
    ],

    models: [
        'Statistics'
    ],

    stores: [
        'Statistics'
    ]
});
