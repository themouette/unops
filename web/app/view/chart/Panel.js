Ext.define('Life.view.chart.Panel', {
    extend: 'Ext.panel.Panel',

    requires: [
        'Ext.chart.Chart',
        'Life.store.Statistics'
    ],

    alias: 'widget.lifechart',

    autoscroll: true,

    title: 'Statistics',

    html: 'Launch simulation to display graphs',

    initComponent: function() {
        this.callParent(arguments);
        Life.simulation.on('start', this.simulationStartListener.bind(this));
        Life.simulation.on('stop', this.simulationStopListener.bind(this));
    },

    simulationStartListener: function () {
        var chartStore = Ext.create('Life.store.Statistics', {
            storeId: 'livechart'
        });
        //create charts
        var chartInstant = this.createInstantChart();
        var chartAllTime = this.createAllTimeChart();
        this.update('');
        this.removeAll();
        this.add(chartInstant);
        this.add(chartAllTime);

        // TODO this should use setTimeout
        // Refresh live chart
        this.statInstantInterval = setInterval(function () {

            var store = Ext.data.StoreManager.lookup('Statistics');
            var chartStore = Ext.data.StoreManager.lookup('livechart');

            // Add a new statistic
            store.add(Life.simulation.getStatistics());
            chartStore.loadData(store.getRange(store.count()-5));

        }, 1000);

        this.redrawAlltimeChartDelegation();
    },
    simulationStopListener: function () {
        clearInterval(this.statInstantInterval);
        clearInterval(this.statAllTimeInterval);
    },

    redrawAlltimeChart: function () {
        var store = Ext.data.StoreManager.lookup('Statistics');
        var data = store.getRange();
        store.loadData(data);
        this.redrawAlltimeChartDelegation();
    },

    redrawAlltimeChartDelegation: function () {
        this.statAllTimeInterval = setTimeout(this.redrawAlltimeChart.bind(this), 2000);
    },

    createInstantChart: function () {
        return Ext.create('Ext.chart.Chart', {
            title: 'Live chart',
            style: 'background:#fff',
            theme: 'Category1',
            layout: 'fit',
            width: 500,
            height: 300,
            store: Ext.data.StoreManager.lookup('livechart'),
            fields: [
                { name: 'grass', type: 'int' },
                { name: 'zebra', type: 'int' },
                { name: 'lion', type: 'int' },
                { name: 'bacteria', type: 'int' },
                { name: 'timestamp' },
            ],
            legend: {
                position: 'bottom'
            },
            axes: [{
                type: 'Numeric',
                minimum: 0,
                position: 'left',
                fields: ['grass', 'zebra', 'lion', 'bacteria'],
                title: 'Populations',
                //minorTickSteps: 1,
                grid: {
                    odd: {
                        opacity: 1,
                        fill: '#ddd',
                        stroke: '#bbb',
                        'stroke-width': 0.5
                    }
                }
            }, {
                type: 'Time',
                position: 'bottom',
                fields: 'timestamp',
                title: 'Date',
                step: [Ext.Date.SECOND, 5],
                constrain: true,
                dateFormat: 'H:m:s'
            }],

            series: [
                {
                    type: 'line',
                    xField: 'timestamp',
                    yField: 'grass'
                }, {
                    type: 'line',
                    xField: 'timestamp',
                    yField: 'zebra'
                }, {
                    type: 'line',
                    xField: 'timestamp',
                    yField: 'lion'
                }, {
                    type: 'line',
                    xField: 'timestamp',
                    yField: 'bacteria'
                }
            ],
            items: [{
                type  : 'text',
                text  : 'Live Chart',
                font  : '14px Arial',
                width : 100,
                height: 30,
                x : 50, //the sprite x position
                y : 250  //the sprite y position
            }]
        });
    },

    createAllTimeChart: function () {
        return Ext.create('Ext.chart.Chart', {
            title: 'Evolution over time',
            style: 'background:#fff',
            theme: 'Category1',
            layout: 'fit',
            width: 500,
            height: 300,
            store: Ext.data.StoreManager.lookup('Statistics'),
            fields: [
                { name: 'grass', type: 'int' },
                { name: 'zebra', type: 'int' },
                { name: 'lion', type: 'int' },
                { name: 'bacteria', type: 'int' },
                { name: 'timestamp' },
            ],
            legend: {
                position: 'bottom'
            },
            axes: [{
                type: 'Numeric',
                minimum: 0,
                position: 'left',
                fields: ['grass', 'zebra', 'lion', 'bacteria'],
                title: 'Populations',
                //minorTickSteps: 1,
                grid: {
                    odd: {
                        opacity: 1,
                        fill: '#ddd',
                        stroke: '#bbb',
                        'stroke-width': 0.5
                    }
                }
            }, {
                type: 'Time',
                position: 'bottom',
                fields: 'timestamp',
                title: 'Date',
                step: [Ext.Date.SECOND, 5],
                constrain: true,
                dateFormat: 'H:m:s'
            }],

            series: [
                {
                    type: 'line',
                    xField: 'timestamp',
                    yField: 'grass'
                }, {
                    type: 'line',
                    xField: 'timestamp',
                    yField: 'zebra'
                }, {
                    type: 'line',
                    xField: 'timestamp',
                    yField: 'lion'
                }, {
                    type: 'line',
                    xField: 'timestamp',
                    yField: 'bacteria'
                }
            ],
            items: [{
                type  : 'text',
                text  : 'Population Evolution Over Time',
                font  : '14px Arial',
                width : 200,
                height: 30,
                x : 50, //the sprite x position
                y : 250  //the sprite y position
            }]
        });
    }
});
