Ext.define('Life.view.log.Grid' ,{
    extend: 'Ext.grid.Panel',
    alias: 'widget.lifelog',

    plugins: {
        ptype: 'bufferedrenderer'
    },

    title: 'Logs',

    store: 'logs',

    initComponent: function() {
        this.columns = [
            {header: 'Namespace',  dataIndex: 'namespace',  flex: 1},
            {header: 'Event Name', dataIndex: 'eventname', flex: 1},
            {header: 'Date', dataIndex: 'timestamp', flex: 1}
        ];

        this.callParent(arguments);
    }
});
