Ext.define('Life.view.Main', {
    extend: 'Ext.container.Container',
    requires:[
        'Ext.tab.Panel',
        'Ext.layout.container.Border'
    ],

    xtype: 'app-main',

    layout: {
        type: 'border'
    },

    items: [{
        region: 'south',
        xtype: 'panel',
        height: 80,
        html: [
            '<p style="text-align: center;"><a href="run-tests.html" target="_blank">Unit Tests</a> | <a href="http://muetton.me">muetton.me</a> | <a href="http://unops.deploy.muetton.me/README.markdown">Notes</a></p>'
        ],
        collapsible: true
    },{
        title: 'Technical test for UNOPS, by Julien Muetton on april 2014.',
        region: 'center',
        xtype: 'tabpanel',
        items:[{
            xtype: 'lifesimulation'
        },{
            xtype: 'lifelog'
        },{
            xtype: 'lifechart'
        }],

        tbar: [
            {
                xtype: 'button',
                text: 'Start',
                listeners: {
                    click: function () {
                        // toggle simulation start/stop
                        var simulation = Life.simulation;
                        if (simulation.isStarted()) {
                            simulation.stop();
                            this.setText('Start');
                            return ;
                        }
                        // start.
                        this.setText('Stop');
                        simulation.start();
                    }
                }
            },
            {
                xtype: 'button',
                text: 'Pause',
                listeners: {
                    click: function () {
                        Ext.Msg.alert('Not implemented', 'This feature is not implemented yet.');
                    }
                }
            }
        ]
    }]
});
