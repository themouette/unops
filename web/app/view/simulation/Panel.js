Ext.define('Life.view.simulation.Panel', {
    extend: 'Ext.panel.Panel',

    requires: [
        'Ext.Msg'
    ],

    alias: 'widget.lifesimulation',

    title: 'Simulation',

    html: [
        '<p>Grass population count: <span id="grasscount"></span></p>',
        '<p>Zebra population count: <span id="zebracount"></span></p>',
        '<p>Lion population count: <span id="lioncount"></span></p>',
        '<p>Bacteria population count: <span id="bacteriacount"></span></p>',
    ].join(''),

    initComponent: function() {
        this.callParent(arguments);
        Life.simulation.on('start', this.simulationStartListener.bind(this));
    },

    simulationStartListener: function () {
        var mediator = Life.simulation.mediator;
        mediator.on('grass', 'countchanged', function (newCount, simulation) {
            // Set the new count in HTML
            document.getElementById('grasscount').innerHTML = newCount;
        });
        mediator.on('zebra', 'countchanged', function (newCount, simulation) {
            // Set the new count in HTML
            document.getElementById('zebracount').innerHTML = newCount;
        });
        mediator.on('lion', 'countchanged', function (newCount, simulation) {
            // Set the new count in HTML
            document.getElementById('lioncount').innerHTML = newCount;
        });
        mediator.on('bacteria', 'countchanged', function (newCount, simulation) {
            // Set the new count in HTML
            document.getElementById('bacteriacount').innerHTML = newCount;
        });
    }
});
