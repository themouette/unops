Ext.define('Life.model.Statistics', {
    extend: 'Ext.data.Model',

    fields: [
        { name: 'grass', type: 'int' },
        { name: 'zebra', type: 'int' },
        { name: 'lion', type: 'int' },
        { name: 'bacteria', type: 'int' },
        { name: 'timestamp' }
    ]
});

