Ext.define('Life.model.Organism', {
    extend: 'Ext.data.Model',

    requires: [
        'Life.lib.Logger'
    ],

    fields: [
        { name: 'name', type: 'string' },
        { name: 'lifeExpectancy', type: 'integer' },
        { name: 'birthRate', type: 'integer' },
        { name: 'eats', type: 'Array' }
    ],

    constructor: function (values, config) {

        this.callParent(arguments);

        config || (config = {});

        // Create a new logger for this sequence
        var logger = this.logger = Ext.create('Life.lib.Logger', {
            store: config.store || Ext.data.StoreManager.lookup('logs'),
            namespace: this.get('name')
        });

        // program death
        this.programDeath();
        // program become eatable
        this.programBecomeEatable();
    },

    log: function () {
        this.logger.log.apply(arguments);
    },

    // It would be cleaner to use the model observable interface instead of
    // injecting mediator.
    //
    // TODO fix this.
    // TODO remove events on model destruction.
    listenToSimulationChange: function (mediator) {
        mediator.addListener('simulation', 'pause', this.pauseListener.bind(this));
        mediator.addListener('simulation', 'unpause', this.unpauseListener.bind(this));
        mediator.addListener('simulation', 'stop', this.stopListener.bind(this));
    },

    pauseListener: function() {
        // delay timers until resume
    },
    unpauseListener: function() {
        // resume timers
    },
    stopListener: function() {
        this.die();
    },

    programDeath: function () {
        this.deathTimer = setTimeout(this.die.bind(this), this.get('lifeExpectancy'));
    },

    programBecomeEatable: function () {
        this.eatableTimer = setTimeout(this.becomeEatable.bind(this), this.get('lifeExpectancy') / 2);
    },

    die: function(organism) {
        this.clearTimers();
        this.dead = true;
        this.fireEvent('die', [this]);
        this.destroy();
    },

    becomeEatable: function () {
        this.fireEvent('becomeEatable', [this]);
    },

    clearTimers: function() {
        clearTimeout(this.deathTimer);
        clearTimeout(this.eatableTimer);
    },

    isDead: function () {
        return this.dead === true;
    }
});
