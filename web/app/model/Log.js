Ext.define('Life.model.Log', {
    extend: 'Ext.data.Model',

    fields: [
        { name: 'namespace', type: 'string' },
        { name: 'eventname', type: 'string' },
        { name: 'timestamp', type: 'Date' }
    ]
});
