Ext.define('Life.store.Organisms', {
    extend: 'Ext.data.Store',
    model: 'Life.model.Organism',
});
