describe('Simulation', function () {
    var simulation, logs;
    beforeEach(function () {
        // Create a logs store.
        //
        // Simply access to it using
        //
        // ``` javascript
        // Ext.data.StoreManager.lookup('logs');
        // ```
        logs = Ext.create('Life.store.Logs', {});

        simulation = Ext.create('Life.lib.Simulation', {
            logStore: logs
        });
    });

    afterEach(function () {
        logs.destroyStore();
    });

    function itShouldTrigger(event) {
        var spy = sinon.spy();
        simulation.on(event, spy);
        simulation[event]();
        expect(spy.calledOnce).toBeTruthy();
    }

    describe('#start()', function () {
        it('should trigger `start` event', function () {
            itShouldTrigger('start');
        });
        it('should start timers'/*, function () {

        }*/);
    });
    describe('#stop()', function () {
        it('should trigger `stop` event', function () {
            simulation.start();
            itShouldTrigger('stop');
        });
        it('should stop all timers'/*, function () {

        }*/);
        it('should stop kill all populations'/*, function () {

        }*/);
    });
    describe('#pause()', function () {
        it('should trigger `pause` event', function () {
            simulation.start();
            itShouldTrigger('pause');
        });
        it('should pause timers'/*, function () {

        }*/);
        it('should keep populations'/*, function () {

        }*/);
    });
    describe('#unpause()', function () {
        it('should trigger `unpause` event', function () {
            simulation.start();
            itShouldTrigger('unpause');
        });
        it('should unpause'/*, function () {

        }*/);
        it('should keep populations'/*, function () {

        }*/);
    });
    describe('#togglePause()', function () {
        it('should toggle pause'/*, function () {

        }*/);
    });
});
