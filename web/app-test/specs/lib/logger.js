describe('Life.lib.Logger', function() {

    var logger, store;

    beforeEach(function () {
        store = Ext.create('Life.store.Logs');
        logger = Ext.create('Life.lib.Logger', {
            store: store
        });
    });

    describe('constructor', function () {
        it('should keep a reference to given store', function () {
            expect(logger.store).toBe(store);
        });

        it('should throw an error if no store is provided', function () {
            expect(function () {
                logger = Ext.create('Life.lib.Logger', {});
            }).toThrow();
        });

        it('should throw an error if provided store is null', function () {
            expect(function () {
                logger = Ext.create('Life.lib.Logger', {
                    store: null
                });
            }).toThrow();
        });
    });

    describe('#log()', function () {
        it('should append a new log to store', function () {
            expect(store.count()).toBe(0);

            logger.log('tardis', 'varnish');

            expect(store.count()).toBe(1);
            expect(store.first().get('namespace')).toBe('tardis');
            expect(store.first().get('eventname')).toBe('varnish');
        });

        it('should be possible to pass a default namespace', function () {
            logger = Ext.create('Life.lib.Logger', {
                store: store,
                namespace: 'tardis'
            });

            logger.log('varnish');

            expect(store.count()).toBe(1);
            expect(store.first().get('namespace')).toBe('tardis');
            expect(store.first().get('eventname')).toBe('varnish');
        });

        it('should throw an error if no store is provided', function () {

        });
    });
});
