describe('Mediator class', function() {

    var mediator;

    beforeEach(function () {
        mediator = Ext.create('Life.lib.Mediator');
    });

    describe('#registerEvent()', function () {
        it('should accept new events', function () {
            mediator.registerEvent('foo', 'bar');
        });
        it('should store observables in #namespaces', function () {
            expect(mediator.namespaces.foo).toBeUndefined();

            mediator.registerEvent('foo', 'bar');

            expect(mediator.namespaces.foo).toBeDefined();
        });
        it('should keep already registered events', function () {
            mediator.registerEvent('foo', 'bar');
            var foo = mediator.namespaces.foo;
            mediator.registerEvent('foo', 'baz');

            expect(mediator.namespaces.foo).toBe(foo);
        });
        it('should not require eventname', function () {
            mediator.registerEvent('foo');

            expect(mediator.namespaces.foo).toBeDefined();
        });

        it('should be chainable', function () {
            expect(mediator.registerEvent('foo')).toBe(mediator);
            expect(mediator.registerEvent('foo', 'bar')).toBe(mediator);
        });

        // This test is due to my lack of knowledge about Ext classes.
        // I wanted to check if properties were static or dynamic.
        it('should define a set of observable per instance', function () {
            var mediatorA = Ext.create('Life.lib.Mediator');
            var mediatorB = Ext.create('Life.lib.Mediator');

            mediatorA.registerEvent('foo', 'bar');
            mediatorB.registerEvent('bar', 'baz');

            expect(mediatorA.namespaces.foo).toBeDefined();
            expect(mediatorA.namespaces.bar).not.toBeDefined();

            expect(mediatorB.namespaces.foo).not.toBeDefined();
            expect(mediatorB.namespaces.bar).toBeDefined();
        });
    });

    describe('#addListener()', function() {

        var spy;

        beforeEach(function () {
            mediator.registerEvent('foo', 'bar');
            spy = function () {};
        });

        it('should be chainable', function () {

            expect(mediator.addListener('foo', 'bar', spy)).toBe(mediator);
            expect(mediator.addListener('foo', 'bar', spy)).toBe(mediator);
        });

        it('should define handlers', function () {
            expect(hasListeners(mediator, 'foo', 'bar')).toBeFalsy();

            mediator.addListener('foo', 'bar', spy);

            expect(hasListeners(mediator, 'foo', 'bar')).toBeTruthy();

            mediator.addListener('foo', 'bar', spy);
        });

        // A helper funciton to check if an event has listeners.
        function hasListeners(mediator, ns, eventname) {
            return mediator.namespaces[ns].hasListener(eventname);
        }
    });

    describe('#fireEvent()', function() {

        var spy;

        beforeEach(function () {
            mediator.registerEvent('foo', 'bar');
            spy = sinon.spy();
        });

        it('should be chainable', function () {
            expect(mediator.fireEvent('foo', 'bar')).toBe(mediator);
        });

        it('should trigger if no handlers is defined', function () {
            expect(function () {
                mediator.fireEvent('foo', 'bar');
            }).not.toThrow();
        });

        it('should trigger handler', function () {
            mediator.addListener('foo', 'bar', spy);

            mediator.fireEvent('foo', 'bar');

            expect(spy.calledOnce).toBeTruthy();
        });

        it('should trigger all handlers', function () {
            var spy2 = sinon.spy();
            mediator.addListener('foo', 'bar', spy);
            mediator.addListener('foo', 'bar', spy2);

            mediator.fireEvent('foo', 'bar');

            expect(spy.callCount).toBe(1);
            expect(spy2.callCount).toBe(1);
        });

        // This is a native feature on Ext.util.Observable.
        it('should trigger handler only once (avoid double registration)', function () {
            mediator.addListener('foo', 'bar', spy);
            mediator.addListener('foo', 'bar', spy);

            mediator.fireEvent('foo', 'bar');

            expect(spy.callCount).toBe(1);
        });

        it('should trigger handlers for events only', function () {
            var spy2 = sinon.spy();

            mediator.registerEvent('foo', 'bazinga');

            mediator.addListener('foo', 'bar', spy);
            mediator.addListener('foo', 'bazinga', spy2);

            mediator.fireEvent('foo', 'bar');

            expect(spy.called).toBeTruthy();
            expect(spy2.called).toBeFalsy();
        });

        it('should trigger handlers for namespace only', function () {
            var spy2 = sinon.spy();

            mediator.registerEvent('bazinga', 'bar');

            mediator.addListener('foo', 'bar', spy);
            mediator.addListener('bazinga', 'bar', spy2);

            mediator.fireEvent('foo', 'bar');

            expect(spy.called).toBeTruthy();
            expect(spy2.called).toBeFalsy();
        });
    });


});
