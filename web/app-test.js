Ext.require('Ext.app.Application');

var Application = null;

Ext.onReady(function() {
    // Define class mapping.
    Ext.Loader.addClassPathMappings({
        'Life': 'app'
    });
    Application = Ext.create('Ext.app.Application', {
        name: 'Life',

        controllers: [
        ],

        launch: function() {
            //include the tests in the test.html head
            jasmine.getEnv().addReporter(new jasmine.TrivialReporter());
            jasmine.getEnv().execute();
        }
    });
});
