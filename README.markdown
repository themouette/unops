UNOPS technical test
====================

This is the UNOPS test project by Julien Muetton <julien@muetton.me>

## Notes

A deployed version is [available online](http://unops.deploy.muetton.me/).

Application is available in the `web/app` directory.

Ext js is embedded, so nothing should be missing.

It was my first look into Ext since 2008, so I took the opportunity to give it a
try.
I stopped unit testing in the process, as long term maintainability is not
expected there, and time was running out.

I noticed a bug when simulation turns into a massive extinction, an error
occurs, unfortunately it is too late to fix it.

Chart axis should be fixed at every recalculation to give user a better feeling.

I tested under Firefox Aurora and Chromium 33.

## Project organisation

Application is available in the `web` directory.

Tests are written with [Jasmine
1.3](http://jasmine.github.io/1.3/introduction.html).

Test files are located in `web/app-test/specs` folder.

### Libraries

Following libraries are provided:

* `Life.lib.Mediator` is the main event dispatcher used to communicate between
    every application components
* `Life.lib.Simulation` is the simulation utility.
* `Life.lib.Logger` is the logger utility.

### Simulation

Simulation service manages populations lifcycle.

It works as a state machine for `pause` and `start` and provide a Mediator to
Populations.
Populations listen to Simulation state events and respond to it.

### Population

A Population has a store of Organisms and all the population configuration:

* name: the population name;
* birthRate: time for a new organism birth;
* lifeExpectancy: duration before an Organism dies;
* eat: an array of populations this one eat.

It responds to mediator events, spread new organism according to `birthRate`
configuration option and manage feeding events.

When an Organism become eatable, Population publish the event on the mediator.

Population listen for every new eatable Organism, and if the organism is alive,
good to eat and Population achieved to catch it, it is eaten.

Population publish through mediator any population count change.

### Statistics

On application start, Life.view.chart.Panel starts a timer to sample simulation
statistics.

Statistics store stores all statistics sampled by Life.view.chart.Panel.

Both graphs are updated by reloading associated store.

## Installation

**This is only for development purpose**

### Sencha

Install [Sencha Cmd](http://www.sencha.com/products/sencha-cmd/download) and
install it into `vendor/` directory.

[Download ExtJS 4.2.1](http://cdn.sencha.com/ext/gpl/ext-4.2.1-gpl.zip) and
unzip it into `vendor/Sencha/` directory.

You're done. Check it by runing `./sencha` command.

### Phantomjs

Phantomjs should be available. Most simple way is to use npm:

``` sh
npm install -g phantomjs
```

### Development server

Install required packages using npm:

```
npm install
```

To start server, just use `npm start`. Server is listening on port 3000.

## Available commands

### Sencha Cmd

Sencha CMD and Sencha ExtJS are shipped locally as vendor dependencies.
It is available in the `vendor` directory.

From root directory, simply run `./sencha` command to execute sencha commands,
this is a local wrapper for local sencha command installation.

``` sh
./sencha help
```

### Tests

To launch tests with phantom, just run

``` sh
test
```

Tests are done using Jasmine and SinonJS.
Refer to [ExtJS testing cookbook](http://docs.sencha.com/extjs/4.2.1/#!/guide/testing) for further information.
